# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url, handler404

from django.contrib import admin
admin.autodiscover()

from django.conf import settings
from aurora_app import views

from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm

from django.views.decorators.csrf import csrf_exempt

#handler404 = views.error_404

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^productos/$', views.productos, name='productos'),
	url(r'^productos/page/(\d+)/$', views.productos_pagina, name='productos_pagina'),
	url(r'^categoria/(\d+)/([A-Za-z0-9\-]+)/$', views.categoria, name='categoria'),
	url(r'^categoria/(\d+)/([A-Za-z0-9\-]+)/page/(\d+)/$', views.categoria_pagina, name='categoria_pagina'),
	#url(r'^buscar/([-A-Za-z0-9+&@#/%=~_| \'"]+)/$', views.buscar, name='buscar'),
	url(r'^buscar/(.*)/$', views.buscar, name='buscar'),

	url(r'^mi-cuenta/$', views.cuenta, name='cuenta'),
	url(r'^mi-cuenta/page/(\d+)/$', views.cuenta_page, name='cuenta_page'),
	url(r'^mi-cuenta/configuracion/$', views.configuracion, name='configuracion'),
	url(r'^mi-cuenta/configuracion/actualizado/$', views.configuracion_actualizado, name='configuracion_actualizado'),
	url(r'^mi-cuenta/actualizar/personal/$', views.actualizar_personal, name='actualizar_personal'),
	url(r'^mi-cuenta/actualizar/direccion/$', views.actualizar_direccion, name='actualizar_direccion'),
	url(r'^login/$', views.loginn, name='login'),
	url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
	url(r'^registro/$', views.registro, name='registro'),

	url(r'^articulo/(\d+)/', views.articulo, name='articulo'),
	url(r'^votos/(\d+)/$', views.votar, name='votar'),
	url(r'^carrito/$', views.carrito, name='carrito'),
	url(r'^eliminar/(\d+)/$', views.delcarrito, name='delcarrito'),
	url(r'^pedido/(\d+)/$', views.pedido, name='pedido'),

	url(r'^pagar/$', views.pagar, name='pagar'),
	url(r'^pagar/pedido/(\d+)/$', views.pagar_pedido, name='pagar_pedido'),
	url(r'^pagar/pedido/payu/(\d+)/$', views.pagar_pedido_dineromail, name='pagar_pedido_dineromail'),

	url(r'^distribuidor/$', views.mayoreo, name='mayoreo'),
	url(r'^faqs/$', views.faqs, name='faqs'),
	url(r'^about/$', views.aboutus, name='aboutus'),
	url(r'^contacto/$', views.contacto, name='contacto'),
	url(r'^reset/$', views.reset, name='reset'),
	url(r'^reset/(\d+)/([A-Za-z0-9]+)/$', views.reset_activate, name='reset_activate'),

	url(r'^pagado/(\d+)/([A-Za-z0-9]+)/$', views.pagado, name='pagado'),
	url(r'^cancelado/(\d+)/([A-Za-z0-9]+)/$', views.cancelado, name='cancelado'),

	url(r'^endpoint/$', csrf_exempt(views.MyEndPoint()), name='paypal'),

	url(r'^admin/', include(admin.site.urls)),

)

if settings.DEBUG:
	# static files (images, css, javascript, etc.)
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))