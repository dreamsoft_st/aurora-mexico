"""
Django settings for aurora_mx project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'grw955rzk-*1w1exb%_*1n)x$1ekk4twzg^4)90--pd87n#=r&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = []

ADMINS  = (('Victor', 'crymore3@gmail.com'), )


# Application definition

INSTALLED_APPS = (
	'suit',
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'south',
	'colorful',
	'aurora_app',
)

MIDDLEWARE_CLASSES = (
#	'django.middleware.cache.UpdateCacheMiddleware',

	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',
	'aurora_app.middleware.AutoLogout',

#	'django.middleware.cache.FetchFromCacheMiddleware',
)
CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True

ROOT_URLCONF = 'aurora_mx.urls'

WSGI_APPLICATION = 'aurora_mx.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'aurora_db',
		'USER': 'aurora_db',
		'PASSWORD': 'aurora_db_123.',
		'HOST': 'web351.webfaction.com',
		'PORT': '5432'
	}
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'America/Mazatlan'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

TEMPLATE_DIRS = (
	os.path.join(BASE_DIR, 'aurora_app/templates'),
)
STATICFILES_DIRS = (
	os.path.join(BASE_DIR, 'aurora_app/static'),
)

#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_ROOT = '/home/s00rk/webapps/aurora_statics/'
STATIC_URL = '/static/'

MEDIA_ROOT = '/home/s00rk/webapps/aurora_media/'
MEDIA_URL = 'http://auroramexico.com.mx/media/'

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
	'django.core.context_processors.request',
	"aurora_app.processors.menu"
)


AUTH_PROFILE_MODULE = 'aurora_app.DatoCliente'

#Admin Config

SUIT_CONFIG = {
	'ADMIN_NAME': 'Aurora Mexico',
	'HEADER_TIME_FORMAT': 'h:i a',
	'MENU_EXCLUDE': ('auth.group', 'auth'),
	'LIST_PER_PAGE': 50,
}

from django.core.urlresolvers import reverse_lazy

LOGIN_URL = reverse_lazy('login')
LOGIN_REDIRECT_URL = reverse_lazy('productos')

LOGOUT_URL = reverse_lazy('logout')

SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
AUTO_LOGOUT_DELAY = 10 # minutos
AUTO_DELETE_PEDIDO = 2 # dias

SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'

EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'dreamsoft'
EMAIL_HOST_PASSWORD = 'saske321.'
DEFAULT_FROM_EMAIL = 'Auroraleds <contacto.dreamsoft.st>'
SERVER_EMAIL = 'contacto.dreamsoft.st'


EMAIL_ADMIN = 'crymore3@gmail.com'

EMAIL_PAYPAL = 'danielfelixt@hotmail.com'

