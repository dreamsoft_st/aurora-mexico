# -*- coding: utf-8 -*-
from django.http import HttpResponse
import urllib
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

class Endpoint:
	
	default_response_text = ''
	verify_url = "https://www.sandbox.paypal.com/cgi-bin/webscr"
	
	def do_post(self, url, args):
		for k, v in args.iteritems():
			args[k] = unicode(v).encode('utf-8')
		return urllib.urlopen(url, urllib.urlencode(args)).read()
	
	def verify(self, data):
		args = {
			'cmd': '_notify-validate',
		}
		args.update(data)
		verificar = self.do_post(self.verify_url, args)
		return verificar == 'VERIFIED'
	
	def default_response(self):
		return HttpResponse(self.default_response_text)
	
	def __call__(self, request):
		r = None
		if request.method == 'POST':
			data = dict(request.POST.items())
			resp = self.verify(data)
			if resp:
				r = self.process(data)
			else:
				r = self.process_invalid(data)
		if r:
			return r
		else:
			return self.default_response()
	
	def process(self, data):
		pass
	
	def process_invalid(self, data):
		pass

class AppEngineEndpoint(Endpoint):
	
	def do_post(self, url, args):
		from google.appengine.api import urlfetch
		return urlfetch.fetch(
			url = url,
			method = urlfetch.POST,
			payload = urllib.urlencode(args)
		).content