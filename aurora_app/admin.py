from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.auth.models import User

from aurora_app.models import *
from aurora_app.actions import export_as_csv, download_csv
#from suit_ckeditor.widgets import CKEditorWidget

class DatoClienteInline(admin.TabularInline):
	model = DatoCliente
	exclude = ['domicilio', 'reset', 'reset_times']
	readonly_fields = ('Domicilio', 'telefono')

	def Domicilio(selff, obj):
		self = obj.domicilio
		return "Calle: %s<br>No. Exterior: %s<br>No. Interior: %s<br>Colonia: %s<br>Ciudad: %s<br>Estado: %s<br>CodigoPostal: %s" % \
			(self.calle, self.no_exterior, self.no_interior, self.colonia, self.ciudad, self.estado, self.codigoPostal)
	Domicilio.allow_tags = True

class UserAdmin(DjangoUserAdmin):
	inlines = (DatoClienteInline,)
	actions = [download_csv]


class CategoriaAdmin(admin.ModelAdmin):
	search_fields = ('nombre',)
	list_display=('id','nombre','descripcion')
	search_fields = ('nombre',)
	actions = [export_as_csv]
	list_display_links = ('nombre',)

class ImagenInline(admin.StackedInline):
	model = Imagenes
	extra = 4

class ArticuloAdmin(admin.ModelAdmin):
	list_display=('id','nombre','precio','costo','inventario','id_categoria','votos')
	actions = [export_as_csv]
	search_fields = ('nombre',)
	raw_id_fields = ('id_categoria',)
	inlines = [ImagenInline]
	list_display_links = ('nombre',)
	list_editable = ('inventario',)
	
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

class ImagenesAdmin(admin.ModelAdmin):
	list_display=('id','img','id_articulo')
	raw_id_fields = ('id_articulo',)
	search_fields = ('id_articulo__nombre',)
	list_display_links = ('id_articulo',)

	def img(self, obj):
		return '<img src="/media/%s" width="200">' % obj.imagen
	img.allow_tags = True

class LPedidoInline(admin.StackedInline):
	model = LineaPedido
	extra = 0

class PedidoAdmin(admin.ModelAdmin):
	list_display=('id','fecha','estado','id_cliente')
	raw_id_fields = ('id_cliente',)
	inlines = [LPedidoInline]
	readonly_fields=('Domicilio', 'fecha')
	search_fields = ('id_cliente__username', 'estado',)
	exclude = ('domicilio', 'clabe')
	list_editable = ('estado',)
	list_display_links = ('fecha',)

	def Domicilio(selff, obj):
		self = obj.domicilio
		return "Calle: %s<br>No. Exterior: %s<br>No. Interior: %s<br>Colonia: %s<br>Ciudad: %s<br>Estado: %s<br>CodigoPostal: %s" % \
			(self.calle, self.no_exterior, self.no_interior, self.colonia, self.ciudad, self.estado, self.codigoPostal)
	Domicilio.allow_tags = True

	actions = ['delete_model', export_as_csv]

	def get_actions(self, request):
		actions = super(PedidoAdmin, self).get_actions(request)
		del actions['delete_selected']
		return actions

	def delete_model(self, request, obj):
		for o in obj.all():
			pedidos = LineaPedido.objects.filter(id_pedido=o.id)
			for ped in pedidos:
				ped.id_articulo.inventario = int(ped.id_articulo.inventario) + int(ped.unidades)
				ped.id_articulo.save()
				ped.delete()
			o.delete()

	delete_model.short_description = 'Eliminar Seleccionados'


	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

class LPedidoAdmin(admin.ModelAdmin):
	list_display=('id_pedido','id_articulo','unidades')
	raw_id_fields = ('id_pedido', 'id_articulo')

	actions = ['delete_model', export_as_csv]

	def get_actions(self, request):
		actions = super(LPedidoAdmin, self).get_actions(request)
		del actions['delete_selected']
		return actions

	def delete_model(self, request, obj):
		for o in obj.all():
			articulo = o.id_articulo
			articulo.inventario = int(articulo.inventario) + int(o.unidades)
			articulo.save()
			o.delete()

	delete_model.short_description = 'Eliminar Seleccionados'

	search_fields = ('id_pedido__nombre', 'id_pedido__id_cliente__username', 'id_articulo__nombre')

class ContactoAdmin(admin.ModelAdmin):
	list_display = ('id', 'nombre', 'telefono', 'facebook', 'twitter')
	list_display_links = ('nombre',)
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

class PreguntaAdmin(admin.ModelAdmin):
	list_display=('id','nombre','email', 'mensaje')
	search_fields = ('nombre', 'email',)
	actions = [export_as_csv]
	list_display_links = ('nombre',)

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

class BannerAdmin(admin.ModelAdmin):
	list_display=('id','img','titulo', 'descripcion')
	list_display_links = ('titulo',)

	def img(self, obj):
		return '<img src="/media/%s" width="200">' % obj.imagen
	img.allow_tags = True 

admin.site.register(Categoria,CategoriaAdmin)
admin.site.register(Articulo,ArticuloAdmin)
admin.site.register(Imagenes,ImagenesAdmin)
admin.site.register(Pedido,PedidoAdmin)
admin.site.register(LineaPedido,LPedidoAdmin)
admin.site.register(Pregunta, PreguntaAdmin)
#admin.site.register(Domicilio)
admin.site.register(Contacto, ContactoAdmin)
admin.site.register(Banner, BannerAdmin)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)