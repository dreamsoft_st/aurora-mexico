# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Domicilio'
        db.create_table(u'aurora_app_domicilio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('calle', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('no_exterior', self.gf('django.db.models.fields.IntegerField')()),
            ('no_interior', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
            ('colonia', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ciudad', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('estado', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('codigoPostal', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'aurora_app', ['Domicilio'])

        # Adding model 'DatoCliente'
        db.create_table(u'aurora_app_datocliente', (
            ('id_cliente', self.gf('django.db.models.fields.related.OneToOneField')(related_name='perfil', unique=True, primary_key=True, to=orm['auth.User'])),
            ('domicilio', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['aurora_app.Domicilio'], null=True)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
        ))
        db.send_create_signal(u'aurora_app', ['DatoCliente'])

        # Adding model 'Categoria'
        db.create_table(u'aurora_app_categoria', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('descripcion', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'aurora_app', ['Categoria'])

        # Adding model 'Articulo'
        db.create_table(u'aurora_app_articulo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('descripcion', self.gf('django.db.models.fields.TextField')()),
            ('precio', self.gf('django.db.models.fields.FloatField')()),
            ('costo', self.gf('django.db.models.fields.FloatField')()),
            ('inventario', self.gf('django.db.models.fields.IntegerField')()),
            ('id_categoria', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['aurora_app.Categoria'])),
            ('votos', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal(u'aurora_app', ['Articulo'])

        # Adding model 'Imagenes'
        db.create_table(u'aurora_app_imagenes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('imagen', self.gf('django.db.models.fields.files.ImageField')(default='leds.jpg', max_length=100)),
            ('id_articulo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['aurora_app.Articulo'])),
        ))
        db.send_create_signal(u'aurora_app', ['Imagenes'])

        # Adding model 'Pedido'
        db.create_table(u'aurora_app_pedido', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateTimeField')()),
            ('estado', self.gf('django.db.models.fields.CharField')(default='solicitado', max_length=30)),
            ('id_cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('comentario', self.gf('django.db.models.fields.TextField')(default='')),
            ('domicilio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['aurora_app.Domicilio'])),
        ))
        db.send_create_signal(u'aurora_app', ['Pedido'])

        # Adding model 'LineaPedido'
        db.create_table(u'aurora_app_lineapedido', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('id_pedido', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['aurora_app.Pedido'])),
            ('id_articulo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['aurora_app.Articulo'])),
            ('unidades', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('precio', self.gf('django.db.models.fields.FloatField')(default=0)),
        ))
        db.send_create_signal(u'aurora_app', ['LineaPedido'])

        # Adding model 'Pregunta'
        db.create_table(u'aurora_app_pregunta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('id_cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('pregunta', self.gf('django.db.models.fields.TextField')()),
            ('respuesta', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'aurora_app', ['Pregunta'])

        # Adding model 'Estado'
        db.create_table(u'aurora_app_estado', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'aurora_app', ['Estado'])


    def backwards(self, orm):
        # Deleting model 'Domicilio'
        db.delete_table(u'aurora_app_domicilio')

        # Deleting model 'DatoCliente'
        db.delete_table(u'aurora_app_datocliente')

        # Deleting model 'Categoria'
        db.delete_table(u'aurora_app_categoria')

        # Deleting model 'Articulo'
        db.delete_table(u'aurora_app_articulo')

        # Deleting model 'Imagenes'
        db.delete_table(u'aurora_app_imagenes')

        # Deleting model 'Pedido'
        db.delete_table(u'aurora_app_pedido')

        # Deleting model 'LineaPedido'
        db.delete_table(u'aurora_app_lineapedido')

        # Deleting model 'Pregunta'
        db.delete_table(u'aurora_app_pregunta')

        # Deleting model 'Estado'
        db.delete_table(u'aurora_app_estado')


    models = {
        u'aurora_app.articulo': {
            'Meta': {'object_name': 'Articulo'},
            'costo': ('django.db.models.fields.FloatField', [], {}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Categoria']"}),
            'inventario': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'precio': ('django.db.models.fields.FloatField', [], {}),
            'votos': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'aurora_app.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'aurora_app.datocliente': {
            'Meta': {'object_name': 'DatoCliente'},
            'domicilio': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['aurora_app.Domicilio']", 'null': 'True'}),
            'id_cliente': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'perfil'", 'unique': 'True', 'primary_key': 'True', 'to': u"orm['auth.User']"}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'aurora_app.domicilio': {
            'Meta': {'object_name': 'Domicilio'},
            'calle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'codigoPostal': ('django.db.models.fields.IntegerField', [], {}),
            'colonia': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'no_exterior': ('django.db.models.fields.IntegerField', [], {}),
            'no_interior': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        },
        u'aurora_app.estado': {
            'Meta': {'object_name': 'Estado'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'aurora_app.imagenes': {
            'Meta': {'object_name': 'Imagenes'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_articulo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Articulo']"}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'default': "'leds.jpg'", 'max_length': '100'})
        },
        u'aurora_app.lineapedido': {
            'Meta': {'object_name': 'LineaPedido'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_articulo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Articulo']"}),
            'id_pedido': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Pedido']"}),
            'precio': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'unidades': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'aurora_app.pedido': {
            'Meta': {'object_name': 'Pedido'},
            'comentario': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'domicilio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Domicilio']"}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'solicitado'", 'max_length': '30'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'aurora_app.pregunta': {
            'Meta': {'object_name': 'Pregunta'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'pregunta': ('django.db.models.fields.TextField', [], {}),
            'respuesta': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['aurora_app']