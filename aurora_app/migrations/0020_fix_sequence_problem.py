# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        pass

    def backwards(self, orm):
        pass

    models = {
        u'aurora_app.articulo': {
            'Meta': {'object_name': 'Articulo'},
            'costo': ('django.db.models.fields.FloatField', [], {}),
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            'descuento': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Categoria']"}),
            'inventario': ('django.db.models.fields.IntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'precio': ('django.db.models.fields.FloatField', [], {}),
            'vendido': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'votos': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        },
        u'aurora_app.banner': {
            'Meta': {'object_name': 'Banner'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'aurora_app.carrito': {
            'Meta': {'object_name': 'Carrito'},
            'articulo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Articulo']"}),
            'cantidad': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'aurora_app.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'descripcion': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'aurora_app.contacto': {
            'Meta': {'object_name': 'Contacto'},
            'aboutus': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'celular': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'direccion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'distribuidor': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'facebook': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'faqs': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'twitter': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'})
        },
        u'aurora_app.datocliente': {
            'Meta': {'object_name': 'DatoCliente'},
            'domicilio': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['aurora_app.Domicilio']", 'null': 'True', 'blank': 'True'}),
            'id_cliente': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'perfil'", 'unique': 'True', 'primary_key': 'True', 'to': u"orm['auth.User']"}),
            'reset': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'reset_times': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'})
        },
        u'aurora_app.domicilio': {
            'Meta': {'object_name': 'Domicilio'},
            'calle': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'codigoPostal': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'colonia': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'estado': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'no_exterior': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'no_interior': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'})
        },
        u'aurora_app.estado': {
            'Meta': {'object_name': 'Estado'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'aurora_app.imagenes': {
            'Meta': {'object_name': 'Imagenes'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_articulo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Articulo']"}),
            'imagen': ('django.db.models.fields.files.ImageField', [], {'default': "'leds.jpg'", 'max_length': '100'})
        },
        u'aurora_app.lineapedido': {
            'Meta': {'object_name': 'LineaPedido'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_articulo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Articulo']"}),
            'id_pedido': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Pedido']"}),
            'precio': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'unidades': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'aurora_app.pedido': {
            'Meta': {'object_name': 'Pedido'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'clabe': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'comentario': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'domicilio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['aurora_app.Domicilio']"}),
            'estado': ('django.db.models.fields.CharField', [], {'default': "'solicitado'", 'max_length': '30'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'total': ('django.db.models.fields.FloatField', [], {'default': '0'})
        },
        u'aurora_app.pregunta': {
            'Meta': {'object_name': 'Pregunta'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'fecha': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 7, 31, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mensaje': ('django.db.models.fields.TextField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['aurora_app']