from django.template import RequestContext
from aurora_app.models import Categoria, Articulo, Contacto, Carrito

def menu(request):
	articulos = Articulo.objects.values('id', 'nombre', 'id_categoria__id').order_by('-nombre')
	menus = Categoria.objects.values('id', 'nombre').order_by('-nombre')
#	arts = request.session.get('articulos', dict())
#	items = len(arts)
	items = 0
	if request.user.is_authenticated():
		items = Carrito.objects.filter(cliente=request.user).count()
	
	contacto = Contacto.objects.all()[:1]
	if contacto.count() == 1:
		contacto = contacto[0]
	else:
		contacto = None

	datos = {'categorias' : menus, 'arts': articulos, 'carrito_items': items, 'cont': contacto}
	return  datos