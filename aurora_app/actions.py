import csv
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.template import Context, Template
from aurora_app.models import DatoCliente

def download_csv(self, request, queryset):
	import csv
	from django.http import HttpResponse
	import StringIO

	f = StringIO.StringIO()
	writer = csv.writer(f)
	writer.writerow(["Usuario", "Email", "Nombre(s)", "Apellidos", "Telefono"])

	for s in queryset:
		telefono = ''
		try:
			telefono = s.perfil.telefono
		except DatoCliente.DoesNotExist:
			pass
		writer.writerow([s.username, s.email, s.first_name, s.last_name, telefono])

	f.seek(0)
	response = HttpResponse(f, content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename=usuarios.csv'
	return response
download_csv.short_description = "Exportar como CSV"


def get_csv_from_dict_list(field_list, data):
	csv_line = ";".join(['{{ row.%s|addslashes }}' % field for field in field_list])
	template = "{% for row in data %}" + csv_line + "\n{% endfor %}"
	return Template(template).render(Context({"data": data}))


def export_as_csv(modeladmin, request, queryset):
	if not request.user.is_staff:
		raise PermissionDenied

	replace_dc = {'\n': '* ', '\r': '', ';': ',', '\"': '|', '\'': '|', 'True': 'Si', 'False': 'No'}
	opts = modeladmin.model._meta
	response = HttpResponse(mimetype='text/csv; charset=utf-8')
	response['Content-Disposition'] = 'attachment; filename=%s.csv' % unicode(opts).replace('.', '_')
	w = csv.writer(response, delimiter=';')
	# import pdb; pdb.set_trace()
	try:
		field_names = modeladmin.model.get_csv_fields()
		v_field_names = field_names
	except:
		field_names = [field.name for field in opts.fields]
		v_field_names = [getattr(field, 'verbose_name') or field.name for field in opts.fields]
	# print field_names
	v_field_names = map(lambda x: x.encode('utf-8') if x != 'ID' else 'Id', v_field_names)

	w.writerow(v_field_names)
	ax = []
	for obj in queryset:
		acc = {}
		for field in field_names:
			try:
				uf = unicode(getattr(obj, field)()).encode('utf-8')
			except:
				try:
					uf = unicode(getattr(obj, field)).encode('utf-8')
				except:
					uf = ''
			for i, j in replace_dc.iteritems():
				uf = uf.replace(i, j)
			if uf == 'None':
				uf = ''
			acc[field] = uf

		ax.append(acc)
	response.write(get_csv_from_dict_list(field_names, ax))
	return response

export_as_csv.short_description = "Exportar como CSV"
