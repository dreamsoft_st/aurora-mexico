from datetime import datetime, timedelta
from django.conf import settings
from django.contrib import auth
from aurora_app.models import Articulo, Carrito, Pedido, LineaPedido
from datetime import datetime
from django.utils import timezone


class AutoLogout:
	def process_request(self, request):
		try:
			cars = Carrito.objects.all()
			for car in cars:
				if timezone.now() - car.fecha > timedelta( 0, (settings.AUTO_LOGOUT_DELAY+5) * 60, 0):
					articulo = car.articulo
					articulo.inventario = int(articulo.inventario) + int(car.cantidad)
					articulo.save()
					car.delete()
		except KeyError:
			pass

		try:
			cars = Pedido.objects.filter(estado='solicitado', tipo='paypal')
			for car in cars:
				if timezone.now() - car.fecha > timedelta( 0, settings.AUTO_DELETE_PEDIDO*60*60, 0):
					pedidos = LineaPedido.objects.filter(id_pedido=car.id)
					for ped in pedidos:
						ped.id_articulo.inventario = int(ped.id_articulo.inventario) + int(ped.unidades)
						ped.id_articulo.save()
						ped.delete()
					car.delete()
		except KeyError:
			pass

		if not request.user.is_authenticated():
			request.session['last_touch'] = datetime.now()
			return

		try:
			if datetime.now() - request.session['last_touch'] > timedelta( 0, settings.AUTO_LOGOUT_DELAY * 60, 0):

				auth.logout(request)
				del request.session['last_touch']

				return
		except KeyError:
			pass

		if request.user.is_authenticated():
			Carrito.objects.filter(cliente=request.user).update(fecha=timezone.now())
		request.session['last_touch'] = datetime.now()