# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404, redirect
from aurora_app.models import *
from aurora_app.forms import *
from django.db.models import Count
from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.utils.encoding import iri_to_uri
from django.utils.http import urlquote
from django.core import serializers
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.auth.models import User
from django.utils import timezone
from django.conf import settings
from datetime import datetime
import math, os, urllib, json, random, string, re

# Create your views here.
#def error_404(request):
#	return render(request, '404.html')

def random_str(size=6, chars=string.ascii_uppercase + string.digits + string.lowercase):
	return ''.join(random.choice(chars) for _ in range(size))

def reset_activate(request, id, uuid):
	if request.user.is_authenticated():
		return redirect('/')
	try:
		cliente = DatoCliente.objects.get(id_cliente=id)
	except DatoCliente.DoesNotExist:
		return render(request, 'reset_error.html')

	if cliente.reset != uuid or len(cliente.reset) == 0:
		return render(request, 'reset_error.html')
	datos = {}
	if request.method == 'POST':
		user = User.objects.get(pk=cliente.id_cliente.id)
		posts = request.POST.copy()

		if posts['password1'] == posts['password2'] and len(posts['password2']) >= 3 and posts['password2'] == posts['password2'].strip():
			user.set_password(posts['password1'])
			user.save()
			user = authenticate(username=user.username, password=posts['password1'])
			login(request, user)
			cliente.reset_times = 0
			cliente.reset = ''
			cliente.save()

			html_content = render_to_string('reset_email2.html', {'usuario': user.username})
			text_content = strip_tags(html_content)
			if user.email:
				msg = EmailMultiAlternatives('Reestablecer Contraseña', text_content, 'Auroraleds<contacto@dreamsoft.st>', [user.email])
				msg.attach_alternative(html_content, "text/html")
				msg.send()

			return redirect('/')

		else:
			datos['error'] = 'Las Contraseñas no coinciden'

	return render(request, 'reset_change.html', datos)

def reset(request):
	if request.user.is_authenticated():
		return redirect('/')
	bien = False
	if request.method == 'POST':
		user = False
		try:
			user = User.objects.get(username=request.POST['username'])
		except User.DoesNotExist:
			return render(request, 'reset.html', {'error': 'El Usuario No Existe'})

		uuid = random_str(40)
		cliente, created = DatoCliente.objects.get_or_create(id_cliente=user.id)

		if int(cliente.reset_times) >= 3:
			return render(request, 'reset.html', {'error': 'Ya se ha enviado un correo para el reestablecimiento de esta cuenta'})

		cliente.reset = uuid
		cliente.reset_times = int(cliente.reset_times) + 1
		cliente.save()

		url = 'http://%s/reset/%s/%s/' % (request.META['HTTP_HOST'], user.id, uuid)

		html_content = render_to_string('reset_email.html', {'usuario': user.username, 'url': url})
		text_content = strip_tags(html_content)
		if user.email:
			msg = EmailMultiAlternatives('Reestablecer Contraseña', text_content, 'Auroraleds<contacto@dreamsoft.st>', [user.email])
			msg.attach_alternative(html_content, "text/html")
			msg.send()
		bien = True

	return render(request, 'reset.html', {'bien': bien})

def loginn(request):
	if request.user.is_authenticated():
		return redirect('/')
	error = ''
	username = ''
	password = ''
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				request.session['last_touch'] = datetime.now()
				login(request, user)
				return redirect(request.GET.get('next', '/'))
			else:
				error = 'La cuenta ha sido desactivada'
		else:
			error = 'Usuario/Contraseña Incorrecta'
	return render(request, 'login.html', {'error': error, 'username': username, 'password': password}) 

def registro(request):
	if request.user.is_authenticated():
		return redirect('/')

	error = ''
	post = {}
	post['user-username'] = ''
	post['user-email'] = ''
	if request.method == 'POST':
		post = post_values = request.POST.copy()
		post['user-date_joined'] = timezone.now()
		post['user-last_login'] = timezone.now()
		post['user-is_active'] = 1
		uf = UserForm(post, prefix='user')
		if User.objects.filter(email=post['user-email']).count() == 0:
			if User.objects.filter(username=post['user-username']).count() == 0:
				if post['user-password'] == post['password'] and len(request.POST['password']) >= 3 and request.POST['password'] == request.POST['password'].strip():
					if uf.is_valid():				
						user = uf.save()
						user.set_password(post['password'])
						user.save()
						cl = DatoCliente()
						cl.id_cliente = user
						cl.save()
						user = authenticate(username=post['user-username'], password=post['user-password'])
						login(request, user)

						html_content = render_to_string('registro_email.html', {'usuario':request.user.username}) # ...
						text_content = strip_tags(html_content)
						if user.email:
							msg = EmailMultiAlternatives('Bienvenido %s' % user.username, text_content, 'Auroraleds<contacto@dreamsoft.st>', [user.email])
							msg.attach_alternative(html_content, "text/html")
							msg.send()

						return redirect('/mi-cuenta/configuracion/')
				else:
					error = 'Contraseñas no son iguales'
			else:
				error = 'Ya existe ese usuario'
		else:
			error = 'Ya existe ese correo'
	return render(request, 'registro.html', {'error': error, 'user': post['user-username'], 'email': post['user-email']})

def index(request):
	arts = Articulo.objects.values('id', 'nombre').order_by('-votos')[:6]
	masvotados = []
	for a in arts:
		img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
		if img.count() == 1:
			img = img[0]['imagen']
			a['imagenes__imagen'] = img
		masvotados.append(a)

	banners = Banner.objects.values('titulo', 'descripcion', 'imagen', 'url').order_by('-id')[:6]
	bann = list(banners)
	for banns in banners:
		if banns['url'] == None or banns['url'] == '':
			banns['url'] = '#'
	if banners[0] != None:
		bann[0]['clase'] = ' active'

	datos = {'masvotados': masvotados, 'banners': banners}

	return render(request,'index.html', datos)

def productos(request):
	total = Articulo.objects.all().count()

	arts = Articulo.objects.values('id', 'nombre').order_by('-nombre')[:6]
	articulos = []
	for a in arts:
		img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
		if img.count() == 1:
			img = img[0]['imagen']
			a['imagenes__imagen'] = img
		articulos.append(a)

	pages = int(math.ceil(total/6.0))
	datos = {'articulos': articulos, 'pages': range(pages), 'actual': 1, 'url_active_pro': 'url_activa'}
	return render(request,'productos.html', datos)

def productos_pagina(request, page):
	total = Articulo.objects.all().count()
	actual = page
	page = int(page)-1
	page = int(page)*6
	until = int(page)+6

	arts = Articulo.objects.values('id', 'nombre').order_by('-nombre')[page:until]
	articulos = []

	for a in arts:
		img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
		if img.count() == 1:
			img = img[0]['imagen']
			a['imagenes__imagen'] = img
		articulos.append(a)

	pages = int(math.ceil(total/6.0))
	datos = {'articulos': articulos, 'pages': range(pages), 'actual': actual, 'url_active_pro': 'url_activa'}

	return render(request,'productos.html', datos)

def categoria(request, cat, nombre):
	total = Articulo.objects.filter(id_categoria=cat).count()

	arts = Articulo.objects.values('id', 'nombre', 'descripcion', 'id_categoria__nombre').filter(id_categoria=cat) \
		.order_by('-nombre')[:6]
	articulos = []
	for a in arts:
		img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
		if img.count() == 1:
			img = img[0]['imagen']
			a['imagenes__imagen'] = img
		articulos.append(a)

	pages = int(math.ceil(total/6.0))
	url = '/categoria/' + cat + '/' + nombre + '/page/'
	datos = {'articulos': articulos, 'cat':cat, 'pages': range(pages), 'url': url, 'actual': 1, 'url_active_pro': 'url_activa'}
	return render(request,'categorias.html', datos)

def categoria_pagina(request, cat, nombre, page):
	total = Articulo.objects.filter(id_categoria=cat).count()
	actual = page
	page = int(page)-1
	page = int(page)*6
	until = int(page)+6

	arts = Articulo.objects.values('id', 'nombre', 'descripcion', 'id_categoria__nombre').filter(id_categoria=cat) \
		.order_by('-nombre')[page:until]
	articulos = []
	for a in arts:
		img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
		if img.count() == 1:
			img = img[0]['imagen']
			a['imagenes__imagen'] = img
		articulos.append(a)

	pages = int(math.ceil(total/6.0))
	url = '/categoria/' + cat + '/' + nombre + '/page/'
	datos = {'articulos': articulos, 'cat':cat, 'pages': range(pages), 'url': url, 'actual': actual, 'url_active_pro': 'url_activa'}
	return render(request,'categorias.html', datos)

def buscar(request, nombre):
	arts = Articulo.objects.values('id', 'nombre', 'descripcion', 'id_categoria__nombre').filter(nombre__contains=nombre) \
		.order_by('-nombre')
	articulos = []
	for a in arts:
		img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
		if img.count() == 1:
			img = img[0]['imagen']
			a['imagenes__imagen'] = img
		articulos.append(a)

	datos = {'articulos': articulos, 'url_active_pro': 'url_activa'}
	return render(request,'buscar.html', datos)

def votar(request, id_articulo):
	articulo = get_object_or_404(Articulo, pk = id_articulo)

	url = iri_to_uri('http://graph.facebook.com/?id=http://%s/articulo/%d/%s/' % (request.META['HTTP_HOST'], int(id_articulo), urlquote(articulo.nombre)))
	response = urllib.urlopen(url)
	data = json.loads(response.read())

	if 'shares' in data:
		articulo.votos = int(data['shares'])
	else:
		articulo.votos = 0	
	articulo.save()

	imagenes = Imagenes.objects.filter(id_articulo=id_articulo).order_by('-id')
	datos = {'articulo': articulo, 'imagenes': imagenes, 'url_active_pro': 'url_activa'}
	return render(request, 'buscar.html')

def articulo(request, id):
	articulo = get_object_or_404(Articulo, pk = id)
	articulo_agregado = False
	error = False
	descuento = ((float(articulo.descuento) * 0.01) * float(articulo.precio))

	if request.method == 'POST':
		
		if not request.user.is_authenticated():
			return redirect('/login/?next=/articulo/' + str(articulo.id) + '/' + articulo.nombre )

		id_articulo = request.POST['id_articulo']
		cantidad = request.POST['cantidad']
#		articulos = request.session.get('articulos', dict())
		articulos = Carrito.objects.filter(cliente=request.user)
		existe = False


		if int(articulo.inventario) >= int(cantidad):
			if int(articulo.id) == int(id_articulo):
				for art in articulos:
					if int(art.articulo.id) == int(id_articulo):
						art.cantidad = int(art.cantidad) + int(cantidad)
						art.save()
						existe = True
				if not existe:
					car = Carrito()
					car.cliente = request.user
					car.articulo = articulo
					car.cantidad = int(cantidad)
					car.fecha = timezone.now()
					car.save()

#				if id_articulo in articulos:
#					articulos[id_articulo] = int(articulos[id_articulo]) + int(cantidad)
#				else:
#					articulos[id_articulo] = cantidad
#				request.session['articulos'] = articulos
				articulo_agregado = True
				articulo.inventario = int(articulo.inventario) - int(cantidad)
				articulo.save()
			else:
				error = 'El articulo solicitado no existe'
		else:
			error = 'La cantidad solicitada sobrepasa la existencia en inventario.'

	imagenes = Imagenes.objects.filter(id_articulo=id).order_by('id')
	datos = {'articulo': articulo, 'descuento': (float(articulo.precio)-descuento), 'imagenes': imagenes, 'articulo_agregado': articulo_agregado, 'url_active_pro': 'url_activa', 'error': error}

	return render(request,'articulo.html', datos)

@login_required
def carrito(request):
#	articulos = request.session.get('articulos', dict())
	articulos = Carrito.objects.filter(cliente=request.user)
	arts = []
	total = 0
	n = 0
	for art in articulos:
		try:
			articulo = art.articulo
			precio = float(articulo.precio) - ((float(articulo.descuento) * 0.01) * float(articulo.precio))
			a = { 
				'id': articulo.id, 'nombre': articulo.nombre, 'cantidad': art.cantidad, 
				'precio': precio, 'total': (float(art.cantidad)*precio),
				'clase': ("success", "info")[n%2 == 0]
			}
			img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
			if img.count() == 1:
				img = img[0]['imagen']
				a['imagen'] = img
			arts.append(a)
			total += (float(art.cantidad)*precio)
			n += 1
		except Articulo.DoesNotExist:
			pass

	return render(request, 'carrito.html', {'articulos': arts, 'total': total})

@login_required
def delcarrito(request, id_articulo):
	#articulos = request.session.get('articulos', dict())
	car = Carrito.objects.filter(articulo=id_articulo, cliente=request.user)
	if car.count() > 0:
		for cc in car:
			art = Articulo.objects.get(pk=cc.articulo.id)
			art.inventario = int(art.inventario) + int(cc.cantidad)
			art.save()
			cc.delete()
#	if id_articulo in articulos:
#		try:
#			articulo = Articulo.objects.get(pk = art)
#			articulo.inventario = int(articulo.inventario) + int(articulos[id_articulo])
#			articulo.save()
#		except Articulo.DoesNotExist:
#			pass
#		del articulos[id_articulo]
#	request.session['articulos'] = articulos
	return render(request, 'buscar.html')

@login_required
def cuenta(request):
	user = request.user
	total = Pedido.objects.filter(id_cliente=user.id).count()
	n = 0

	pedidos = Pedido.objects.values('id', 'fecha', 'estado', 'total', 'tipo').filter(id_cliente=user.id) \
		.order_by('-id')[:6]
	for ped in pedidos:
		if ped['tipo'] == 'vendedor':
			ped['tipo'] = 'Acordar con el Vendedor'
		ped['clase'] = ("success", "info")[n%2 == 0]

	pages = int(math.ceil(total/6.0))
	url = '/mi-cuenta/page/'
	datos = {'pedidos': pedidos, 'pages': range(pages), 'url': url, 'actual': 1}
	return render(request,'cuenta.html', datos)

@login_required
def cuenta_page(request, page):
	user = request.user
	total = Pedido.objects.filter(id_cliente=user.id).count()
	actual = page
	page = int(page)-1
	page = int(page)*6
	until = int(page)+6
	n = 0

	pedidos = Pedido.objects.values('id', 'fecha', 'estado', 'total', 'tipo').filter(id_cliente=user.id) \
		.order_by('-id')[page:until]
	for ped in pedidos:
		if ped['tipo'] == 'vendedor':
			ped['tipo'] = 'Acordar con el Vendedor'
		ped['clase'] = ("success", "info")[n%2 == 0]

	pages = int(math.ceil(total/6.0))
	url = '/mi-cuenta/page/'

	datos = {'pedidos': pedidos,'url': url, 'pages': range(pages), 'actual': actual}
	
	return render(request, 'cuenta.html', datos)

@login_required
def configuracion(request):
	estados = Estado.objects.order_by('-nombre')

	datoCLIENTE = DatoCliente.objects.filter(id_cliente=request.user.id)[:1]
	datos = {}
	if datoCLIENTE.count() == 1:
		domicilio = datoCLIENTE[0].domicilio
		if domicilio != None:
			datos = Domicilio.objects.values('calle', 'no_exterior', 'no_interior', 'colonia', 'ciudad', 'estado', 'codigoPostal').get(pk=domicilio.id)
		datos['telefono'] = datoCLIENTE[0].telefono
		
	user = request.user
	datos['email'] = user.email
	datos['estados'] = estados
	datos['nombre'] = user.first_name
	datos['apellido'] = user.last_name
	datos['usuario'] = user.username
	datos['actualizado'] = False
	return render(request, 'config.html', datos)

@login_required
def configuracion_actualizado(request):
	estados = Estado.objects.order_by('-nombre')

	datoCLIENTE = DatoCliente.objects.filter(id_cliente=request.user.id)
	datos = {}
	if datoCLIENTE.count() == 1:
		domicilio = datoCLIENTE[0].domicilio
		if domicilio != None:
			datos = Domicilio.objects.values('calle', 'no_exterior', 'no_interior', 'colonia', 'ciudad', 'estado', 'codigoPostal').get(pk=domicilio.id)
		datos['telefono'] = datoCLIENTE[0].telefono
		
	user = request.user
	datos['email'] = user.email
	datos['estados'] = estados
	datos['nombre'] = user.first_name
	datos['apellido'] = user.last_name
	datos['usuario'] = user.username
	datos['actualizado'] = True
	return render(request, 'config.html', datos)

@login_required
def actualizar_personal(request):
	if not request.method == 'POST':
		return redirect('/mi-cuenta/configuracion/')

	user = request.user
	telefono = request.POST['telefono']
	cliente, created = DatoCliente.objects.get_or_create(id_cliente=user.id, defaults={'telefono': telefono})
	cliente.telefono = telefono
	cliente.save()
	user.first_name = request.POST['nombre']
	user.last_name = request.POST['apellido']
	user.email = request.POST['email']

	if request.POST['password1'] == request.POST['password2'] and len(request.POST['password2']) >= 3 and request.POST['password2'] == request.POST['password2'].strip():
		user.set_password(request.POST['password1'])

	user.save()

	return redirect('/mi-cuenta/configuracion/actualizado/')

@login_required
def actualizar_direccion(request):
	if not request.method == 'POST':
		return redirect('/mi-cuenta/configuracion/')

	existe = False
	posts = request.POST.copy()
	user = request.user
	cliente, created = DatoCliente.objects.get_or_create(id_cliente=user.id)
	if cliente.domicilio != None:
		existe = True
		domicilio = Domicilio.objects.filter(id=cliente.domicilio.id)
	if existe:
		domicilio = domicilio[0]
		domicilio.calle = posts['calle']
		domicilio.no_exterior = int(posts['no_exterior'])
		if posts['no_interior'].isdigit():
			domicilio.no_interior = int(posts['no_interior'])
		else:
			domicilio.no_interior = None
		domicilio.ciudad = posts['ciudad']
		domicilio.colonia = posts['colonia']
		domicilio.estado = posts['estado']
		domicilio.codigoPostal = int(posts['codigoPostal'])
		domicilio.save()
	else:
		domicilio = Domicilio()
		domicilio.calle = posts['calle']
		domicilio.no_exterior = int(posts['no_exterior'])
		if posts['no_interior'].isdigit():
			domicilio.no_interior = int(posts['no_interior'])
		else:
			domicilio.no_interior = None
		domicilio.ciudad = posts['ciudad']
		domicilio.colonia = posts['colonia']
		domicilio.estado = posts['estado']
		domicilio.codigoPostal = int(posts['codigoPostal'])
		domicilio.save()
		cliente.domicilio = domicilio
		cliente.save()

	return redirect('/mi-cuenta/configuracion/actualizado/')

def contacto(request):
	contacto = Contacto.objects.all()[:1]
	if contacto.count() == 1:
		contacto = contacto[0]

	enviado = False
	if request.method == 'POST':
		pregunta = Pregunta()
		pregunta.nombre = request.POST['nombre']
		pregunta.email = request.POST['email']
		pregunta.mensaje = request.POST['mensaje']
		pregunta.fecha = timezone.now();
		pregunta.save()
		enviado = True
		data = {'fecha': pregunta.fecha, 'nombre': pregunta.nombre, 'email': pregunta.email, 'mensaje': pregunta.mensaje}
		html_content = render_to_string('contacto_email.html', data)
		text_content = strip_tags(html_content)
		msg = EmailMultiAlternatives('CONTACTO Auroraleds', text_content, 'Auroraleds<contacto@dreamsoft.st>', [settings.EMAIL_ADMIN])
		msg.attach_alternative(html_content, "text/html")
		msg.send()


	return render(request, 'contacto.html', {'cont': contacto, 'url_active_cont': 'url_activa', 'enviado': enviado})

def mayoreo(request):
	return render(request, 'mayoreo.html', {'url_active_may': 'url_activa'})
def faqs(request):
	return render(request, 'faqs.html', {'url_active_faqs': 'url_activa'})
def aboutus(request):
	return render(request, 'aboutus.html', {'url_active_abo': 'url_activa'})

@login_required
def pagar(request):
#	articulos = request.session.get('articulos', dict())
	articulos = Carrito.objects.filter(cliente=request.user)
#	if not len(articulos) > 0:
	if not articulos.count() > 0:
		return redirect('/')
	
	datos = {}
	url_cancel = ''
	url_success = ''
	url_notify = ''
	custom_data = ''

	arts = []
	n = 1
	total = 0
	for art in articulos:
#		articulo = Articulo.objects.get(pk = art)
		articulo = art.articulo
		precio = float(articulo.precio) - ((float(articulo.descuento) * 0.01) * float(articulo.precio))
		a = { 
			'id': n, 'nombre':  re.sub(ur'[^A-Za-z0-9\ -]+', '', articulo.nombre), 'cantidad': art.cantidad, 
			'precio': precio, 'idd': articulo.id, 'total': precio*float(art.cantidad)
		}
		total += precio * float(art.cantidad)
		n += 1
		arts.append(a)

	pagar = False
	nuevo_dom = None
	if request.method == 'POST':		
		posts = request.POST.copy()
		del posts['nombre']
		del posts['apellido']
		del posts['pagar']
		del posts['modo_compra']

		nuevo_dom = Domicilio()
		nuevo_dom.calle = posts['calle']
		nuevo_dom.no_exterior = int(posts['no_exterior'])
		if posts['no_interior'].isdigit():
			nuevo_dom.no_interior = int(posts['no_interior'])
		else:
			nuevo_dom.no_interior = None
		nuevo_dom.ciudad = posts['ciudad']
		nuevo_dom.colonia = posts['colonia']
		nuevo_dom.estado = posts['estado']
		nuevo_dom.codigoPostal = int(posts['codigoPostal'])
		nuevo_dom.save()

		pedido = Pedido()
		pedido.nombre = request.POST['nombre']
		pedido.apellido = request.POST['apellido']
		pedido.domicilio = nuevo_dom
		pedido.id_cliente = request.user
		pedido.total = total
		pedido.save()

		for art in arts:
			try:
				arti = Articulo.objects.get(pk=art['idd'])
				nuevo_ped = LineaPedido()
				nuevo_ped.id_pedido = pedido
				nuevo_ped.id_articulo = arti
				nuevo_ped.unidades = art['cantidad']
				nuevo_ped.precio = art['precio']
				nuevo_ped.save()

			except Articulo.DoesNotExist:
				pass

		# Aqui elimina del carrito
		for art in articulos:
			art.delete()

		html_content = render_to_string('pedido_email.html', {'carrito': arts, 'fecha': pedido.fecha, 'total': total, 'usuario': request.user.username})
		text_content = strip_tags(html_content)
		if request.user.email:
			msg = EmailMultiAlternatives('Informacion de la Compra', text_content, 'Auroraleds<contacto@dreamsoft.st>', [request.user.email])
			msg.attach_alternative(html_content, "text/html")
			msg.send()

		if request.POST['modo_compra'] == 'vendedor':
			pedido.tipo = 'vendedor'
			pedido.save()
			data = request.POST.copy()
			data['carrito'] = arts
			data['fecha'] = pedido.fecha
			data['total'] = total
			data['usuario'] = '%s %s' % (pedido.nombre, pedido.apellido)
			data['dom'] = Domicilio.objects.get(pk=pedido.domicilio.id)
			data['tipo'] = 'Acordar con el vendedor'
			html_content = render_to_string('pedido2_email.html', data)
			text_content = strip_tags(html_content)
			msg = EmailMultiAlternatives('Compra Auroraleds', text_content, 'Auroraleds<contacto@dreamsoft.st>', [settings.EMAIL_ADMIN])
			msg.attach_alternative(html_content, "text/html")
			msg.send()
			return redirect('/mi-cuenta/')
		else:
			if request.POST['modo_compra'] == 'payu':
				pedido.tipo = 'payu'
			else:
				pedido.tipo = 'paypal'
			uuid = random_str(40)
			pedido.clabe = uuid
			pedido.save()
			if  pedido.tipo == 'paypal':
				return redirect('/pagar/pedido/%s/' % (pedido.id))
			else:
				return redirect('/pagar/pedido/payu/%s/' % (pedido.id))


	estados = Estado.objects.order_by('-nombre')

	domicilio = DatoCliente.objects.filter(id_cliente=request.user.id)
	if domicilio.count() == 1 and pagar == False:
		if domicilio[0].domicilio != None:
			domicilio = domicilio[0].domicilio
			datos = Domicilio.objects.values('calle', 'no_exterior', 'no_interior', 'colonia', 'ciudad', 'estado', 'codigoPostal').get(pk=domicilio.id)
	elif pagar == True and nuevo_dom != None:
		datos = Domicilio.objects.values('calle', 'no_exterior', 'no_interior', 'colonia', 'ciudad', 'estado', 'codigoPostal').get(pk=nuevo_dom.id)
		
	datos['carrito'] = arts
	user = request.user
	datos['estados'] = estados
	datos['nombre'] = user.first_name
	datos['apellido'] = user.last_name
	datos['pagar'] = pagar
	datos['url_cancel'] = url_cancel
	datos['url_success'] = url_success
	datos['url_notify'] = url_notify
	datos['custom_data'] = custom_data

	return render(request, 'pagar.html', datos)

def pagado(request, id, uuid):	
	return redirect('/mi-cuenta/')

def cancelado(request, id, uuid):
	try:
		pedido = Pedido.objects.get(pk=id)
		if pedido.clabe != uuid:
			return redirect('/mi-cuenta/')

		pedidos = LineaPedido.objects.filter(id_pedido=pedido.id)
		for ped in pedidos:
			articulo = Articulo.objects.get(pk=ped.id_articulo.id)
			articulo.inventario = int(articulo.inventario) + int(ped.unidades)
			articulo.save()

		LineaPedido.objects.filter(id_pedido=pedido.id).delete()
		Domicilio.objects.get(pk=pedido.domicilio.id).delete()
		pedido.delete()


	except Pedido.DoesNotExist, Articulo.DoesNotExist:
		pass
	return redirect('/mi-cuenta/')

@login_required
def pedido(request, id):
	ped = None
	try:
		pedidos = Pedido.objects.get(pk=id)
	except Pedido.DoesNotExist:
		return redirect('/mi-cuenta/')
	if pedidos.id_cliente != request.user:
		return redirect('/')

	articulos = []
	total = 0
	n = 0
	lp = LineaPedido.objects.filter(id_pedido=pedidos.id)
	for art in lp:
		articulo = art.id_articulo
		a = { 
			'id': articulo.id, 'nombre': articulo.nombre, 'cantidad': art.unidades, 
			'precio': art.precio, 'total': (float(art.unidades)*float(art.precio)),
			'clase': ("success", "info")[n%2 == 0]
		}
		img = Imagenes.objects.values('imagen').filter(id_articulo=a['id']).order_by('id')[:1]
		if img.count() == 1:
			img = img[0]['imagen']
			a['imagen'] = img
		articulos.append(a)
		total += (float(art.unidades)*float(art.precio))
		n += 1


	return render(request, 'pedido.html', {'articulos': articulos, 'total': total, 'idd':pedidos.id, 'fecha': pedidos.fecha, 'estado': pedidos.estado, 'tipo': pedidos.tipo})

@login_required
def pagar_pedido(request, id):
	try:
		pedidos = Pedido.objects.get(pk=id)
	except Pedido.DoesNotExist:
		return redirect('/mi-cuenta/')
	if pedidos.id_cliente != request.user or pedidos.estado != 'solicitado':
		return redirect('/')

	articulos = LineaPedido.objects.filter(id_pedido=pedidos.id)

	datos = {}
	arts = []
	n = 1
	total = 0
	for art in articulos:
		articulo = art.id_articulo
		a = { 
			'id': n, 'nombre':  re.sub(ur'[^A-Za-z0-9\ -]+', '', articulo.nombre), 'cantidad': art.unidades, 
			'precio': float(art.precio), 'idd': articulo.id, 'total': float(art.precio)*float(art.unidades)
		}
		total += float(art.precio) * float(art.unidades)
		n += 1
		arts.append(a)

	datos['carrito'] = arts
	datos['url_success'] = 'http://%s/pagado/%s/%s/' % (request.META['HTTP_HOST'], pedidos.id, pedidos.clabe)
	datos['url_cancel'] = 'http://%s/cancelado/%s/%s/' % (request.META['HTTP_HOST'], pedidos.id, pedidos.clabe)
	datos['url_notify'] = 'http://%s/endpoint/' % (request.META['HTTP_HOST'])
	datos['custom_data'] = '%s-%s' % (pedidos.id, pedidos.clabe)
	return render(request, 'pagar_pedido.html', datos)

@login_required
def pagar_pedido_dineromail(request, id):
	try:
		pedidos = Pedido.objects.get(pk=id)
	except Pedido.DoesNotExist:
		return redirect('/mi-cuenta/')
	if pedidos.id_cliente != request.user or pedidos.estado != 'solicitado':
		return redirect('/')

	articulos = LineaPedido.objects.filter(id_pedido=pedidos.id)

	datos = {}
	arts = []
	n = 1
	total = 0
	for art in articulos:
		articulo = art.id_articulo
		a = { 
			'id': n, 'nombre':  re.sub(ur'[^A-Za-z0-9\ -]+', '', articulo.nombre), 'cantidad': art.unidades, 
			'precio': float(art.precio)*100, 'idd': articulo.id, 'total': float(art.precio)*float(art.unidades)
		}
		total += float(art.precio)*100 * float(art.unidades)
		if(int(a['precio']) == float(a['precio'])):
			a['precio'] = int(a['precio'])
		n += 1
		arts.append(a)

	datos['carrito'] = arts
	datos['ok_url'] = 'http://%s/pagado/%s/%s/' % (request.META['HTTP_HOST'], pedidos.id, pedidos.clabe)
	datos['error_url'] = 'http://%s/cancelado/%s/%s/' % (request.META['HTTP_HOST'], pedidos.id, pedidos.clabe)
	datos['custom_data'] = '%s-%s' % (pedidos.id, pedidos.clabe)
	return render(request, 'pagar_pedido_payu.html', datos)

from aurora_app.paypal import Endpoint
from django.http import HttpResponse
class MyEndPoint(Endpoint):
	def process(self, data):

		if data['payment_status'] == 'Completed' and data['receiver_email'] == settings.EMAIL_PAYPAL:
			datos = str(data['custom']).split('-')
			id = datos[0]
			clabe = datos[1]

			try:
				pedido = Pedido.objects.get(pk=id)
				if pedido.clabe != clabe:
					return HttpResponse('VALIDO PERO NO ENCONTRADO')


				totall = (float(pedido.total))
				ttotal = (float(data['mc_gross']))
				if totall != ttotal:
					return HttpResponse('VALIDO PERO NO ENCONTRADO')
				pedido.estado = 'pagado'
				pedido.clabe = ''
				pedido.save()

				data['tipo'] = 'PAGADO POR PAYPAL'
				data['dom'] = Domicilio.objects.get(pk=pedido.domicilio.id)
				data['carrito'] = LineaPedido.objects.filter(id_pedido=pedido.id)
				data['fecha'] = pedido.fecha
				data['total'] = pedido.total
				data['usuario'] = '%s %s' % (pedido.nombre, pedido.apellido)
				html_content = render_to_string('pedido2_email.html', data)
				text_content = strip_tags(html_content)
				msg = EmailMultiAlternatives('Compra Auroraleds', text_content, 'Auroraleds<contacto@dreamsoft.st>', [settings.EMAIL_ADMIN])
				msg.attach_alternative(html_content, "text/html")
				msg.send()
			except Pedido.DoesNotExist:
				pass

		return HttpResponse('VALIDO')
		
	def process_invalid(self, data):
		return HttpResponse('INVALIDO')