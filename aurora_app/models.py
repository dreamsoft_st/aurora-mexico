from django.db import models
from django.db.models import signals
from django.contrib.auth.models import User
from aurora_app.storage import OverwriteStorage
from django.conf import settings
from django.utils import timezone
from django.db import IntegrityError, transaction

from django.core.cache import cache
from django.db.models.signals import post_save
from django.dispatch import receiver
 
from django.contrib.sessions.models import Session
import os, imghdr, uuid
from colorful.fields import RGBColorField
 
@receiver(post_save)
def clear_cache(sender, **kwargs):
	if sender != Session:
		cache.clear()

class Domicilio(models.Model):
	calle = models.CharField(max_length=100, blank=True)
	no_exterior = models.IntegerField(blank=True, null=True, default=None)
	no_interior = models.IntegerField(blank=True, null=True, default=None)
	colonia = models.CharField(max_length=100, blank=True)
	ciudad = models.CharField(max_length=100, blank=True)
	estado = models.CharField(max_length=100, blank=True)
	codigoPostal = models.IntegerField(blank=True, null=True, default=None)

	def __unicode__(self):
		return "Calle: %s, No. Exterior: %s, No. Interior: %s, Colonia: %s, Ciudad: %s, Estado: %s, CodigoPostal: %s" % \
			(self.calle, self.no_exterior, self.no_interior, self.colonia, self.ciudad, self.estado, self.codigoPostal)

class DatoCliente(models.Model):
	id_cliente = models.OneToOneField(User, unique=True, primary_key=True, related_name='perfil')
	domicilio = models.ForeignKey(Domicilio, blank=True, null=True, default=None)
	telefono = models.CharField(max_length=20, blank=True)
	reset = models.CharField(max_length=100, blank=True, null=True)
	reset_times = models.IntegerField(default=0, null=True)

	def __unicode__(self):
		return  "%s" % self.id_cliente.username

def crear_perfil(sender, instance, created, **kwargs):  
	if created:  
	   profile, created = DatoCliente.objects.get_or_create(id_cliente=instance)

signals.post_save.connect(crear_perfil, sender=User)

class Categoria(models.Model):
	nombre = models.CharField(max_length=40)
	descripcion = models.TextField()

	def __unicode__(self):
		return "%s" % self.nombre

class Articulo(models.Model):
	nombre = models.CharField(max_length=40)
	optica = models.CharField(max_length=40, blank=True, default='--')
	led_color = RGBColorField(default='', blank=True, null=True)
	finish_color = RGBColorField(default='', blank=True, null=True)
	tipo_montaje = models.CharField(max_length=40, blank=True, default='--')
	dimension = models.CharField(max_length=40, blank=True, default='--')
	especificaciones = models.TextField(blank=True)
	caracteristicas = models.TextField(blank=True)
	descripcion = models.TextField(blank=True)
	precio = models.FloatField()
	costo = models.FloatField()
	inventario = models.IntegerField()
	id_categoria = models.ForeignKey('Categoria')
	votos = models.PositiveIntegerField(default=0)
	vendido = models.PositiveIntegerField(default=0)
	descuento = models.PositiveIntegerField(default=0)

	def __unicode__(self):
		return "%s" % self.nombre

def path_and_rename(path):
	def wrapper(instance, filename):
		ext = filename.split('.')[-1]
		if instance.pk:
			filename = '{}.{}'.format(instance.pk, ext)
		else:
			filename = '{}.{}'.format(uuid.uuid4().hex, ext)
		return os.path.join(path, filename)
	return wrapper

class Imagenes(models.Model):
	imagen = models.ImageField(upload_to=path_and_rename('imagenes'), default='leds.jpg')
	id_articulo = models.ForeignKey('Articulo')

	class Meta:
		verbose_name_plural = "Imagenes"

	def __unicode__(self):
		return "%s" % self.id_articulo.nombre

	def save(self, *args, **kw):
		if self.pk is not None:
			orig = Imagenes.objects.get(pk=self.pk)
			if orig.imagen != self.imagen:
				try:
					os.remove( os.path.join(settings.MEDIA_ROOT, orig.imagen.name) )
				except OSError:
					pass
		super(Imagenes, self).save(*args, **kw)


class Pedido(models.Model):
	SOLICITADO = 'solicitado'
	PAGADO = 'pagado'
	ENVIADO = 'enviado'
	RECIBIDO = 'recibido'
	estados = (
		(SOLICITADO, SOLICITADO),
		(PAGADO, PAGADO),
		(ENVIADO, ENVIADO),
		(RECIBIDO, RECIBIDO),
	)
	id_cliente = models.ForeignKey(User)
	nombre = models.CharField(max_length=100)
	apellido = models.CharField(max_length=100)
	estado =  models.CharField(max_length=30, choices=estados, default=SOLICITADO)
	comentario = models.TextField(default='')
	domicilio = models.ForeignKey(Domicilio)
	total = models.FloatField(default=0)
	fecha = models.DateTimeField(auto_now_add=True)
	clabe = models.CharField(blank=True, max_length=100)
	tipo = models.CharField(max_length=100,default='')

	def __unicode__(self):
		return "%s" % self.nombre

class LineaPedido(models.Model):
	id_pedido = models.ForeignKey('Pedido')
	id_articulo = models.ForeignKey('Articulo')
	unidades = models.PositiveIntegerField()
	precio = models.FloatField(default=0)

	def __unicode__(self):
		return "%s" % self.id

class Contacto(models.Model):
	nombre = models.CharField(max_length=200)
	telefono = models.CharField(max_length=100, blank=True)
	celular = models.CharField(max_length=100, blank=True)
	direccion = models.TextField(blank=True)
	facebook = models.URLField(blank=True)
	twitter = models.URLField(blank=True)
	distribuidor = models.TextField(blank=True)
	faqs = models.TextField(blank=True)
	aboutus = models.TextField(blank=True)

	class Meta:
		verbose_name_plural = "Contacto"

	def __unicode__(self):
		return "%s" % self.nombre

class Pregunta(models.Model):
	nombre = models.CharField(max_length=100)
	email = models.EmailField()
	mensaje = models.TextField()
	fecha = models.DateTimeField(auto_now_add=True, default=timezone.now())

	def __unicode__(self):
		return "%s" % self.nombre

class Carrito(models.Model):
	cliente = models.ForeignKey(User)
	articulo = models.ForeignKey(Articulo)
	cantidad = models.IntegerField(default=0)
	fecha = models.DateTimeField(auto_now_add=True)

class Estado(models.Model):
	nombre = models.CharField(max_length=100)
	def __unicode__(self):
		return "%s" % self.nombre

class Banner(models.Model):
	titulo = models.CharField(max_length=100)
	descripcion = models.CharField(max_length=100, blank=True)
	imagen = models.ImageField(upload_to=path_and_rename('banner'))
	url = models.URLField(blank=True, default='')

	def __unicode__(self):
		return "%s" % self.titulo

	def save(self, *args, **kw):
		if self.pk is not None:
			orig = Banner.objects.get(pk=self.pk)
			if orig.imagen != self.imagen:
				try:
					os.remove( os.path.join(settings.MEDIA_ROOT, orig.imagen.name) )
				except OSError:
					pass
		super(Banner, self).save(*args, **kw)